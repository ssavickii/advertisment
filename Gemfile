source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
# ruby '2.1.5'
ruby '2.0.0'
# gem 'rails', '4.0.2'
gem 'rails', '4.1.0'

# Database
# gem 'pg', '>= 0.17.1'
gem 'pg', '~> 0.18.1'

# Use sqlite3 as the database for Active Record
# gem 'sqlite3'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'



# FRONT
gem 'jquery-rails'
# gem 'twitter-bootstrap-rails'

gem 'sass-rails', '~> 4.0.0'
# gem 'slim-rails'
gem 'slim'
gem 'coffee-rails', '~> 4.0.0'
gem 'simple_form'
gem 'paperclip', '~> 4.1'
gem 'font-awesome-sass'
gem 'paperclip-rack', require: 'paperclip/rack'
# end

# UTILS
gem 'gon'                                       # Ruby vars in js
gem 'browser'                                   # detect browser instead of old jquery library


# REST Requests
gem 'rest-client', '~> 1.7.2'
gem 'curb', '~> 0.8.6'

# Mobile debugging
gem 'weinre-rails'

# tests
group :development, :test do
  gem 'rspec-rails', '~> 3.0.0.beta'
  gem 'capybara'
  gem 'factory_girl_rails', '~> 4.0'
  gem 'simplecov', :require => false
  gem 'selenium-webdriver'
  gem 'pry-debugger', '~> 0.2.3'
end

group :development do
  gem 'jazz_hands'
  gem 'letter_opener', '~> 1.3.0'                 # emails local
end

group :production do
  gem 'rails_12factor'
end

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
