TRANSLATE_ERRORS = true
EMAIL_TEST = false

EMAIL_FORMAT = {with: /\A([a-z0-9]{1}[a-z0-9_.-]+[a-z0-9]{1})@([a-z0-9_\.-]+)\.([a-z\.]{2,6})\z/}

if Rails.env.in? %w(development staging test)
  ADMIN_EMAILS = %w(ooofff@mail.ru)
end

if Rails.env.production?
  if EMAIL_TEST
    ADMIN_EMAILS = %w(ooofff@mail.ru)
  else
    ADMIN_EMAILS = %w(traffex@yandex.ru ooofff@mail.ru)
  end
end


# PRICING CONF -------------------------------
PAYMENT_TYPE = %w(FIXED_PRICE REVENUE_SHARE)
# отсюда берем default но также это формирует не задисэбленные селекты выбора модели - так что ждобавив сюда еще модель - даем ее выбирать
PAYMENT_MODELS = %w(CPM) # PAYMENT_TYPE = %w(CPM CPC CPA)
# /PRICING CONF ------------------------------