if Rails.env.production?
  RestClient.log = 'log/rest_client.log'
else
  RestClient.log =
      Object.new.tap do |proxy|
        def proxy.<<(message)
          Rails.logger.info message
        end
      end
end


