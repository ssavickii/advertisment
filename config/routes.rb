Bannermaker::Application.routes.draw do
  resources :campaigns

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # LANDING PAGES =======================================
  root 'landing#index'

  namespace :landing do
    get 'advertiser'
    get 'publisher'
    get 'about_us'
    get 'contacts'
  end
  # /LANDING PAGES =======================================


  # AUTH =================================================
  resource :users, only: [] do
    get 'sign_up'
    get 'sign_in'
    delete 'sign_out'
    post 'create'
    post 'login'
  end
  # /AUTH ================================================


  # PUBLISHER ============================================
  namespace :publisher do
    root 'sites#index'

    # get 'some_action', to: 'payments#some_action'       # publisher_some_action_path	          GET	/publisher/some_action(.:format)	          publisher/payments#some_action

    resources :sites
    resources :zones do
      member do
        get 'get_code'
      end
    end


    # some_action_publisher_payments_path	  GET	/publisher/payments/some_action(.:format) 	publisher/payments#some_action
  end
  # /PUBLISHER ===========================================


  # ADVERTISER ============================================
  namespace :advertiser do
    root 'main#index'

    resource :payments, only: [] do
      get 'some_action'
    end
  end
  # /PUBLISHER ===========================================

  # Test =================================================
  get 'othersite', to: 'other_site#index'
  get  'test',    to: 'other_site#test_get'
  post 'test',    to: 'other_site#test_post'
  put  'test',    to: 'other_site#test_put'
  delete 'test',  to: 'other_site#test_delete'
  # /Test ================================================

end
