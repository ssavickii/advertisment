class AdminMailer < ApplicationMailer

  def new_user(user)
    @user = user

    mail(to: ADMIN_EMAILS, subject: 'Зарегистрирован новый пользователь.')
  end

  def test email=false

    mail(to: email ? email : ADMIN_EMAILS, subject: 'Проверка отправки почты.')
  end

  # AdminMailer.test().deliver
end