class ApplicationMailer < ActionMailer::Base
  add_template_helper(MailerHelper)
  layout 'mailer'
  default from: 'noreply@trafficexchange.com'
end