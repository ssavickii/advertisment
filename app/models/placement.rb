class Placement < ActiveRecord::Base

  include ModelsMethods

  DEFAULT_PAYMENT_MODEL = PAYMENT_MODELS[0]

  TYPE = %w(SITE_PLACEMENT NON_STANDARD_SITE_PLACEMENT MOBILE_SITE_PLACEMENT)
  ADUNIT = %w(MEDIUM_RECTANGLE RECTANGLE LEADERBOARD FULL_BANNER SKYSCRAPER WIDE_SKYSCRAPER HALF_PAGE_AD BUTTON_2 MICRO_BAR XXL_BOX CUSTOM_SITE_BANNER_SIZE)


  validates :name, :type, :size_adUnit, presence: true
  validates :rotateInterval, numericality: {less_than: 1000, message: I18n.t('too_big', scope: ['activerecord', 'errors', 'messages'], count: 1000)}, allow_nil: true

  TYPE_SELECT = string_value_underscore TYPE
  ADUNIT_SELECT = string_value_underscore ADUNIT

  def ad_unit_is value
    self.size_adUnit == value
  end
end
