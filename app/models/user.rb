class User < ActiveRecord::Base

  # attr_accessor :password_confirmation
  attr_accessor :current_action
  attr_accessor :hashed_password

  USER_ROLES = %w(ADVERTISER PUBLISHER)

  # LOGIN
  validates :username, presence: true, length: { maximum: 64 }, if: :login_action?
  validates :password, length: {in: 5..64}, if: :login_action?
  validates :role, presence: true, if: :login_action?
  # /LOGIN

  # CREATE
  validates :username,:firstName, :lastName, :role,  presence: true, length: { maximum: 64 }, if: :create_action?

  validates :password, length: {in: 5..64}, confirmation: true, if: :create_action?
  validates :phone, length: {in: 5..20}, if: :create_action?
  validates :email, format: EMAIL_FORMAT, if: :create_action?
  validates :websiteUrl, format: {with: URI.regexp, message: I18n.t(:invalid_format)}, if: :create_action?

  validates_each :phone do |record, attr, value|
    if record.create_action?
      record.errors.add(attr, I18n.t(:invalid_format_phone)) if !value.present? || !value.delete(' ').match(/^\+[0-9]*\([0-9]*\)[0-9]*$/)
    end
  end
  # /CREATE

  %w(login create).each do |action_name|
    define_method "#{action_name}_action?" do
      self.current_action == action_name
    end
  end

  # нужно чтобы в метод валидации передать какой у нас экшн - валидация для экшнов разная
  def valid?(action_name)
    self.current_action = action_name
    super
  end

  USER_ROLES.each do |role_name|
    define_method "role_#{role_name.downcase}?" do
      self.role == role_name
    end
  end
end
