class Site < ActiveRecord::Base

  include ModelsMethods

  attr_accessor :createDefaultZone

  validates :name, :categoryId, presence: true

  validates :email, format: EMAIL_FORMAT
  validates :url, format: {with: URI.regexp, message: I18n.t(:invalid_format)}
  validates :revenueShare, numericality: {less_than: 100, message: I18n.t('too_big', scope: ['activerecord', 'errors', 'messages'], count: 100)}, allow_nil: true

  COUNT_TYPES = %w(LESS_THAN_100K _100K_1_MILLION _1_MILLION_50_MILLION _50_MILLION_100_MILLION _100_PLUS_MILLION NOT_YET_LAUNCHED DONT_KNOW)

  IMPRESSIONS_BY_MONTH = COUNT_TYPES
  VISITORS_BY_MONTH    = COUNT_TYPES

  IMPRESSIONS_BY_MONTH_SELECT = string_value_underscore IMPRESSIONS_BY_MONTH
  VISITORS_BY_MONTH_SELECT    = string_value_underscore VISITORS_BY_MONTH

  DEFAULT_PAYMENT_MODEL = PAYMENT_MODELS[0]
  # насильно ставим CPM пока что
  CPM_ONLY = true

  CREATE_DEFAULT_ZONE = false

  # по дефолту создается сайт с payment model FIXED_PRICE  payment_type CPM
  # убираем revenueShare в controller action! 0 слать нельзя

  # ENTER BY USER - REQUIRE
  # name                                           – site name (mandatory)
  # url                                            – site url (mandatory)
  # categoryId                                     – id of category site belongs to (mandatory)

  # OPTIONAL
  # description                                    – a short site characteristic (optional)
  # email                                          – contact email (optional)
  # allowPlacementBannersLinkingChange             – boolean value to allow user to perform banner to placements linking itself (optional)
    # ! Пока не используем
  # revenueShare                                   – float between 0 and 100 to represent revenue share percent for given site (optional)
  # impressionsByMonth                             – estimation for impressions served per-month,can be one of following (optional): LESS_THAN_100K,_100K_1_MILLION,_1_MILLION_50_MILLION,_50_MILLION_100_MILLION, _100_PLUS_MILLION,NOT_YET_LAUNCHED,DONT_KNOW (mandatory).
  # visitorsByMonth                                – estimation for number of unique visitors of site within month, can be one of following (optional): LESS_THAN_100K,_100K_1_MILLION,_1_MILLION_50_MILLION,_50_MILLION_100_MILLION, _100_PLUS_MILLION,NOT_YET_LAUNCHED,DONT_KNOW (mandatory).

  # ADDITIONAL API PARAMS
  # hash                                           – a MD5 digest of concatenated username’s md5 password and the given timestamp
  # timestamp                                      – UNIX timestamp used in hash generation
  # username                                       – the given username
  # createDefaultZone                              – boolean value that allows to create default zone with name matching site’s name right along with creating site(optional) itself


  def impressions_by_month_is value
    self.impressionsByMonth == value
  end

  def visitors_by_month_is value
    self.visitorsByMonth == value
  end

  %w(visitors impressions).each do |type|
    define_method "#{type}_by_month_is" do |value|
      self.public_send("#{type}ByMonth") == value
    end
  end

end
