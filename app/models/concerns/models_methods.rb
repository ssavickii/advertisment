module ModelsMethods
  extend ActiveSupport::Concern

  module ClassMethods

    def string_value_underscore const

      # string like:  SITE PLACEMENT
      const.map { |val| {string: val.gsub('_', ' ').squish, value: val} }

      # string like:  Site placement
      #const.map { |val| {string: val.gsub('_', ' ').squish.downcase.camelize, value: val} }
    end

  end
end