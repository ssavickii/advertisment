module ApplicationHelper

  def link_with_active_class(name, path)
    link_to name, path, class: current_page?(path) ? 'active' : '', title: name
  end
end
