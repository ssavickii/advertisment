module MailerHelper
  def mailer_link_to(body, *args, **kwargs)
    link_to styled_link_text(body), *args, kwargs.merge(target: '_blank')
  end

  def mailer_mail_to(email_address, name = nil, *args, **kwargs)
    mail_to email_address, styled_link_text(name || email_address), *args, kwargs.merge(target: '_blank')
  end

  private

  def styled_link_text(link_text)
    content_tag(:font, link_text, color: '#4B89DC', style: 'text-decoration: underline;')
  end
end
