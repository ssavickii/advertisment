class UsersController < ApplicationController

  include SignupApi
  include LoginApi
  include PublisherSitesApi

  layout 'landing'

  def sign_up
    @user = User.new
  end

  def sign_in
    @user = User.new
  end

  def create
    @user = User.new(sign_up_params)

    # validation
    if @user.valid?(action_name)
      # api sign_up request
      api_params = sign_up_api_params @user.attributes
      @response = register_user(api_params)
      response_handler
    else
      @validate_errors = true
    end

    if @api_success
      # result handle
      # ! redirect on login page
      # or auto login and redirect after

      # письмо админу о новом пользователе - он должен его активировать вручную
      AdminMailer.new_user(@user).deliver

      #  Сообщить пользователю о успешно реге и о том что его аккаунт будет активирован в теч-ии определенного времени
      api_success_handler({redirect_to: root_path}, t(:registration_success_and_need_confirm))
    else
      errors_resolver(@user, {render: :sign_up})
    end


    return if performed?
  end

  def login
    @user = User.new(sign_in_params)

    # validation
    if @user.valid?(action_name)
      # api sign_in request
      api_params = sign_in_api_params @user.attributes
      @response = get_token(api_params)
      response_handler
    else
      @validate_errors = true
    end

    if @api_success
      # result handle
      if @response['authToken'].present?
        #  Если мы получили токен - это значит по сути что пара [username, password] верная - поэтому считаем юзера залогиненным у нас в системе

        if role_approve
          create_session @user
          api_success_handler({redirect_to: role_root_path}, t(:signed_in))
        else
          @custom_msg = t(:check_credentials)
          errors_resolver(@user, {render: :sign_in})
        end

      else
        @custom_msg = t(:server_error)
        errors_resolver(@user, {render: :sign_in})
      end
    else
      errors_resolver(@user, {render: :sign_in})
    end

    return if performed?
  end

  def login_with_token token
    # не используем - это логинит юзера в админке epom
    api_method = "/rest-api/auth/#{token}/login.do"
    url = api_url(api_method, true)
    api_success_handler({redirect_to: url}, 'Redirect to login')

    return if performed?
  end

  def sign_out
    if params[:username] == current_user[:username]
      session.delete('current_user')
      redirect_to root_path
    else
      redirect_to request.referrer, flash: {error: t(:cant_destroy_session)}
    end
  end

  private

  def sign_up_params
    params.require(:user).permit(
        :username,
        :password,
        :password_confirmation,
        :email,
        :role,
        :phone,
        :firstName,
        :lastName,
        :websiteUrl,
        :company,
        :country,
        :enable_market_integration
    )
  end

  def sign_up_api_params params
    params.slice(
      'username',
      'password',
      'email',
      'role',
      'phone',
      'firstName',
      'lastName',
      'websiteUrl',
      'company',
      'country',
      'state',
      'enable_market_integration'
    )
  end

  def sign_in_params
    params.require(:user).permit(
        :username,
        :password,
        :role
    )
  end

  def sign_in_api_params params
    params.slice(
        'username',
        'password'
    )
  end

  def role_approve
    set_api_defaults
    case @user.role
      when 'PUBLISHER'
        get_sites(@user.username, md5_hash(@user.password))
        true if (@response_code != 403)
      when 'ADVERTISER'
        get_sites(@user.username, md5_hash(@user.password))
        true if (@response_code == 403)
      else
        false
    end
  end

  def create_session user
    session['current_user'] = {
        username: user.username,
        hashed_password: md5_hash(user.password),
        role: user.role,
        email: user.email,
        phone: user.phone,
        firstName: user.firstName,
        lastName: user.lastName,
        websiteUrl: user.websiteUrl,
        company: user.company,
        country: user.country,
        state: user.state,
        enable_market_integration: user.enable_market_integration
    }.to_json

    # TODO нужен devise помимо очевидных причин нельзя просто по апи получить данные юзера и в сессию их не запишешь
  end

  def role_root_path
    public_send("#{@user.role.downcase}_root_path")
  end
end