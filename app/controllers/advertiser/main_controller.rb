class Advertiser::MainController < ApplicationController

  before_action :check_access
  before_action :check_custom_flash, only: :index

  layout 'application'

  def index
  end

  def check_access
    redirect_to sign_in_users_path, flash: { error: t(:unauthenticated, scope: [:devise, :failure]) } if (!current_user || !current_user.role_advertiser?)
  end

end