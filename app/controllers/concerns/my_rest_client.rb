module MyRestClient
  require 'rest_client'

  def post_request(url, params=nil)
    begin
      response = RestClient.post(url, params) #, {'content-type' => 'application/x-www-form-urlencoded'}
    rescue => e
      puts 'catch error: '
      puts e.try(:response)
      nil
    end
    response
  end

  def get_request(url, params=nil)
    begin
      response = RestClient.get(url, {params: params})
    rescue => e
      puts 'catch error: '
      puts e.try(:response)
      nil
    end
    response
  end
end


# Can Upload !

# RestClient.post( url,
#                  {
#                      :transfer => {
#                          :path => '/foo/bar',
#                          :owner => 'that_guy',
#                          :group => 'those_guys'
#                      },
#  !!!                    :upload => {
#                          :file => File.new(path, 'rb')
#                      }
#                  })