module MainApi

    require 'digest/md5'

    # include MyRestClient
    include CurlClient

    PUBLIC_KEY = 'f5e64b8ee33f68fc'
    PRIVATE_KEY = '49f3f4f43b2c29dd'
    ROLE = %w(ADVERTISER PUBLISHER)
    EPOM_API_PATH = 'https://n68.epom.com'
    EPOM_API_PATH_HTTP = 'http://n68.epom.com'

    def given_timestamp
      DateTime.now.strftime('%Q')
    end

    def set_timestamp
      @timestamp = DateTime.now.strftime('%Q')
    end

    def build_url_params params
      params.to_param
    end

    def api_url(api_method, insecure=false)
      if insecure
        "#{EPOM_API_PATH_HTTP}#{api_method}"
      else
        "#{EPOM_API_PATH}#{api_method}"
      end
    end

    # для запросов авторизованным юзером
    def md5_for_pass password
      md5_hash("#{md5_hash(password)}#{@timestamp}");
    end

    def md5_for_hashed_pass hashed_password
      md5_hash("#{hashed_password}#{@timestamp}");
    end

    def md5_hash str
      Digest::MD5.hexdigest(str)
    end

  def get_hashed_password
    current_user.hashed_password
  end

  def get_username
    current_user.username
  end
end