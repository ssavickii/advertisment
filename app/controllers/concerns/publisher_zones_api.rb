module PublisherZonesApi

  include MainApi

  def get_zones(username, hashed_password, siteId)
    set_api_defaults
    set_timestamp
    api_method = "/rest-api/sites/#{siteId}/zones.do"
    url = api_url api_method

    api_params = {
        hash: md5_for_hashed_pass(hashed_password),
        timestamp: @timestamp,
        username: username
    }

    get_request(url, api_params)
  end

  def create_or_update_zone(api_params, action_str='update')
    set_api_defaults
    set_timestamp

    api_method = '/rest-api/zones/update.do'
    url = api_url api_method


    api_params['hash'] = md5_for_hashed_pass(get_hashed_password)
    api_params['timestamp'] =  @timestamp
    api_params['username'] =  get_username

    # id никакой не отсылать! даже пустой
    api_params.delete 'id' if action_str == 'create'
    api_params.delete 'created_at'
    api_params.delete 'updated_at'

    post_request(url, api_params)
  end

  def delete_zone(id)
    set_api_defaults
    set_timestamp
    api_method = "/rest-api/zones/#{id}/delete.do"
    url = api_url api_method

    api_params = {
        hash: md5_for_hashed_pass(get_hashed_password),
        timestamp: @timestamp,
        username: get_username
    }

    post_request(url, api_params)
  end

end

