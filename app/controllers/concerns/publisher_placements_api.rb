module PublisherPlacementsApi

  include MainApi

  def create_or_update_placement(api_params, action_str='update')
    set_api_defaults
    set_timestamp

    api_method = '/rest-api/placements/update/standard.do'
    url = api_url api_method


    api_params['hash'] = md5_for_hashed_pass(get_hashed_password)
    api_params['timestamp'] =  @timestamp
    api_params['username'] =  get_username

    # id никакой не отсылать! даже пустой
    api_params.delete 'id' if action_str == 'create'
    api_params.delete 'rotateInterval' unless api_params['rotateInterval']

    api_params['size.adUnit'] = api_params['size_adUnit']
    if api_params['size_adUnit'] == 'CUSTOM_SITE_BANNER_SIZE'
      api_params['size.height'] = api_params['size_height']
      api_params['size.width']  = api_params['size_width']
    end

    # api_params['size.adUnit'] = 'CUSTOM_SITE_BANNER_SIZE'
    # api_params['size.height'] = 222
    # api_params['size.width']  = 222

    api_params.delete 'size_adUnit'
    api_params.delete 'size_height'
    api_params.delete 'size_width'
    api_params.delete 'created_at'
    api_params.delete 'updated_at'
    # api_params['allowVariableBannerSizes'] = true # Flexible Placement Size - видимо что придет от адвертисера то и отобразится любых размеров - есть в админке


    # binding.pry

    # binding.pry
    post_request(url, api_params)
  end

  def get_zone_placements(id)
    set_api_defaults
    set_timestamp
    api_method = "/rest-api/zones/#{id}/placements.do"
    url = api_url api_method

    api_params = {
        hash: md5_for_hashed_pass(get_hashed_password),
        timestamp: @timestamp,
        username: get_username
    }

    get_request(url, api_params)
  end
end

