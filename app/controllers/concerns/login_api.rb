module LoginApi

  include MainApi

  def get_token params
    api_method = "/rest-api/auth/token.do"
    url = api_url api_method
    post_request(url, params)
  end

  # не используется - мы не логиним юзеров в интерфейск epom
  def login_by_token token
    api_method = "/rest-api/auth/#{token}/login.do"
    url = api_url(api_method, true)
    # редирект на url залогинит юзера в админке epom
  end
end
