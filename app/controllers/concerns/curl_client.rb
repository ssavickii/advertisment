module CurlClient

  def post_request(url, params=nil)
    @curl_result = Curl.post(url, params) do |opts|
      opts.verbose = true
      opts.ssl_verify_peer = false
      opts.on_redirect {|data| set_response_code data}
      opts.on_missing  {|data| set_response_code data}
      opts.on_failure  {|data| set_response_code data}
      opts.on_success  {|data| set_response_code data}
    end
    response = @curl_result.body_str
    puts response

    @response_code == 200 ? response : nil
  end

  def get_request(url, params=nil)
    @curl_result = Curl.get("#{url}?#{build_url_params(params)}") do |opts|
      opts.verbose = true
      opts.ssl_verify_peer = false
      opts.on_redirect {|data| set_response_code data}
      opts.on_missing  {|data| set_response_code data}
      opts.on_failure  {|data| set_response_code data}
      opts.on_success  {|data| set_response_code data}
    end
    response = @curl_result.body_str
    puts response

    @response_code == 200 ? response : nil
  end

  # check pu & delete requests!
  def put_request(url, params=nil)
    @curl_result = Curl.put(url, params) do |opts|
      opts.verbose = true
      opts.ssl_verify_peer = false
      opts.on_redirect {|data| set_response_code data}
      opts.on_missing  {|data| set_response_code data}
      opts.on_failure  {|data| set_response_code data}
      opts.on_success  {|data| set_response_code data}
    end
    response = @curl_result.body_str
    puts response

    @response_code == 200 ? response : nil
  end

  def delete_request(url, params=nil)
    @curl_result = Curl.delete(url, params) do |opts|
      opts.verbose = true
      opts.ssl_verify_peer = false
      opts.on_redirect {|data| set_response_code data}
      opts.on_missing  {|data| set_response_code data}
      opts.on_failure  {|data| set_response_code data}
      opts.on_success  {|data| set_response_code data}
    end
    response = @curl_result.body_str
    puts response

    @response_code == 200 ? response : nil
  end

  def set_response_code data
    @response_code = data.response_code
  end
end