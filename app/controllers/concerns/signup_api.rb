module SignupApi

  include MainApi

  def register_user params
    @params = clean params
    set_timestamp
    post_request(url, @params)
  end

  def url
    hash = md5_hash_for_sign_up
    "#{EPOM_API_PATH}/rest-api/register-user/#{PUBLIC_KEY}/#{hash}/#{@timestamp}.do"
  end

  def md5_hash_for_sign_up
    # MD5 digest biult on concatenation of username, password, email, private Epom Server key and a timestamp
    Digest::MD5.hexdigest("#{@params[:username]}#{@params[:password]}#{@params[:email]}#{PRIVATE_KEY}#{@timestamp}")
  end

  def clean params
    obj = {}
    params.map{|key, val| obj[key.to_sym] = val if val.present?}
    obj
  end

end


# url like https://n29.epom.com/rest-api/register-user/{key}/{hash}/{timestamp}.do

# Here {key} represents Epom Server public key,
# {timestamp} is a current Unix timestamp on requesting system(in milliseconds)
# and {hash} is a MD5 digest biult on concatenation of username, password, email, private Epom Server key and a timestamp(in milliseconds) in exact aformentioned order.
