module HttpClient

  require 'net/http'

  def http_do url_string
    url = URI.parse url_string
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }

    puts res.body
  end

  def http_send url_string
    uri = URI.parse url_string
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.set_form_data(@params)
    response = http.request(request)

    render :json => response.body
  end

end
