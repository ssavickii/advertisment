module PublisherSitesApi

  include MainApi

  def get_sites(username, hashed_password)
    set_api_defaults
    set_timestamp
    api_method = '/rest-api/sites.do'
    url = api_url api_method

    api_params = {
        hash: md5_for_hashed_pass(hashed_password),
        timestamp: @timestamp,
        username: username
    }

    get_request(url, api_params)
  end

  def create_or_update_site(api_params, action_str='update')
    set_timestamp

    api_method = '/rest-api/sites/update.do'
    url = api_url api_method


    api_params['hash'] = md5_for_hashed_pass(get_hashed_password)
    api_params['timestamp'] =  @timestamp
    api_params['username'] =  get_username
    api_params['createDefaultZone'] =  Site::CREATE_DEFAULT_ZONE
    api_params['categoryId'] = api_params['categoryId'].join(',')

    # id никакой не отсылать! даже пустой
    api_params.delete 'id' if action_str == 'create'
    api_params.delete 'created_at'
    api_params.delete 'updated_at'

    post_request(url, api_params)
  end

  def delete_site(site_id)
    set_api_defaults
    set_timestamp
    api_method = "/rest-api/sites/#{site_id}/delete.do"
    url = api_url api_method

    api_params = {
        hash: md5_for_hashed_pass(get_hashed_password),
        timestamp: @timestamp,
        username: get_username
    }

    post_request(url, api_params)
  end

  def get_publishing_categories
    set_api_defaults
    set_timestamp
    api_method = "/rest-api/categories/publishing.do"
    url = api_url api_method

    api_params = {
        hash: md5_for_hashed_pass(get_hashed_password),
        timestamp: @timestamp,
        username: get_username
    }

    get_request(url, api_params)
  end
end

