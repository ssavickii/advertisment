class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token

  before_action :authenticate_if_not_dev, :detect_browser, :set_api_defaults, :set_defaults


  def authenticate_if_not_dev
    if Rails.env.in? ['staging', 'production']
      authenticate_or_request_with_http_basic 'Please enter login and password' do |name, password|
        name == 'site' && password == 'traffex'
      end
    end
  end

  private
  def detect_browser
    browser = Browser.new()
    if browser.ie?
      gon.browser = {ie: true}
    else
      gon.browser = {ie: false}
    end
  end

  def set_api_defaults
    @api_success = false
    @error_msg = false
    @rest_error_msg = false
    @response_errors = false
    @api_request_error = false
    @validate_errors = false
    @response = nil
    @custom_msg = nil
    @force_json_response  = false
    @response_code = nil
    @timestamp = nil
    @some_json_error = false
  end

  def response_handler
    if @response
      @response = JSON.parse(@response)

      if @response.is_a? Array
        @api_success = true
      else
        if @response['success']
          @api_success = true
        else
          if @response['rest_error']
            @rest_error_msg = true
          elsif @response['error']
            @error_msg = true
          elsif @response['errors']
            @response_errors = true
          else
            @some_json_error = true
          end
        end
      end

    else
      @api_request_error = true
    end
  end

  def form_errors_handler(action, errors, form_data_action)

    respond_to do |format|
      format.html {
        gon.form_errors = errors
        gon.form_data_action = form_data_action

        if action[:render]
          render action[:render]
        else
          # если вдруг почему-то пришли ошибки формы, но мы поставили, что при ошибке нужен редирект - то даем его выполнить с дефолтным сообщением
          flash.now[:error] = t(:server_error)
          redirect_to  action[:redirect_to] if action[:redirect_to]
        end
      }

      format.json { render json: {success: false, form_errors: errors, form_data_action: form_data_action} }
    end
  end

  def show_api_msg(action, msg)
    if @force_json_response
      render json: {success: false, msg: msg ? msg : nil}
    end
    return if @force_json_response

    respond_to do |format|
      format.html {
        if action[:render]
          flash.now[:error] = msg if msg
          render action[:render]
        else
          flash[:error] = msg if msg
          redirect_to  action[:redirect_to] if action[:redirect_to]
        end
      }

      format.json {

        # session[:custom_flash] = {msg: msg, type: 'error'}.to_json if action[:redirect_to]
        render json: {success: false, msg: msg ? msg : nil}
      }
    end

  end

  def api_success_handler(action, msg)
    respond_to do |format|
      format.html {
        if action[:render]
          flash.now[:alert] = msg if msg
          render action[:render]
        else
          flash[:alert] = msg if msg
          redirect_to  action[:redirect_to] if action[:redirect_to]
        end
      }

      format.json {
        # session[:custom_flash] = {msg: msg, type: 'notice'}.to_json if action[:redirect_to]
        render json: {success: true, action: action, msg: msg, response: @response}
      }
    end
  end

  def translate_errors
    if TRANSLATE_ERRORS
      locale_errors = {}
      @response['errors'].each  do |k, v|
        msgs = Array v
        locale_msgs = msgs.map{ |msg| t(msg.squeeze.tr(' ', '_').to_sym) }
        locale_errors[k] = locale_msgs
      end
      locale_errors
    else
      errors
    end
  end

  def rest_error_msg
    if TRANSLATE_ERRORS
      t(@response['rest_error'].squeeze.tr(' ', '_').to_sym)
    else
      @response['rest_error']
    end
  end

  def error_msg
    if TRANSLATE_ERRORS
      t(@response['error'].squeeze.tr(' ', '_').to_sym)
    else
      @response['error']
    end
  end

  def get_json_msg
    msgs =  @response.map { |key, val| key if (key != 'success') }.reject(&:blank?)

    if TRANSLATE_ERRORS
      msgs = msgs.map{|msg| t(msg.to_sym)}
    end

    msgs.join(' | ')
  end

  # js validation form data-action
  def form_data_action
    [controller_name, action_name].join('_')
  end

  def set_defaults
    # gon['locale'] = ru
    # gon['default_error_msg'] =  '...'
    gon.locale = locale.to_s

    error_key = gon.locale + '_default_error_msg'
    msg = Rails.cache.fetch(error_key) { t(:default_error_msg) }

    gon.push({defaults: {
        default_error_msg: msg
        # ...
        }
    })

  end

  def errors_resolver(obj, action)
    if  @rest_error_msg
      show_api_msg(action, rest_error_msg)
    elsif  @error_msg
      show_api_msg(action, error_msg)
    elsif @response_errors
      form_errors_handler(action, translate_errors, form_data_action)
    elsif @api_request_error
      show_api_msg(action, t(:server_error))
    elsif @custom_msg
      show_api_msg(action, @custom_msg)
    elsif @some_json_error
      show_api_msg(action, get_json_msg)
    else @validate_errors
      form_errors_handler(action,  obj ? obj.errors : nil, form_data_action)
    end
  end

  def check_custom_flash
    if session[:custom_flash]
      flash_obj = JSON.parse(session[:custom_flash])
      msg = flash_obj['msg']
      type = flash_obj['type'].to_sym
      flash[type] = msg
      session.delete(:custom_flash)
    end
  end

  def current_user
    user = session['current_user']
    if user
      attrs = JSON.parse(user)
      @current_user = User.new(attrs)
    end
  end

  def set_obj_name
    @obj_name_plur = self.controller_name
    @obj_name = @obj_name_plur.singularize
  end
  # -------------------------------------------

end
