class LandingController < ApplicationController

  layout 'landing'

  def index
  end

  def advertiser
  end

  def publisher
  end

  def about_us
  end

  def contacts
  end
end
