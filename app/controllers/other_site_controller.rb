class OtherSiteController < ApplicationController

  layout false

  protect_from_forgery :only => [:index]
  # skip_before_filter :verify_authenticity_token

  def index
  end

  %w(get post put delete).each do |type|
    define_method "test_#{type}" do
      render text: "ok_test_#{type}"
    end
  end

end
