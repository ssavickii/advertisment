class Publisher::SitesController < Publisher::MainController

  before_action :set_obj_name, only: [:index, :edit, :destroy]

  def index
    @form_data_action = "#{@obj_name_plur}_create"
    @form_id = "#{@form_data_action}_form"
    @form_method = 'post'

    # для создания нового объекта в модальном окне
    instance_variable_set("@#{@obj_name}", @obj_name.camelize.constantize.new)

    # Категории для сайта
    get_categories

    # GET SITES for PUBLISHER
    @response = get_sites(current_user.username, current_user.hashed_password)
    response_handler

    if @api_success
      @sites = @response
      @sites = @sites.uniq
      set_sites_hash_by_id(@sites)
    else
      errors_resolver(nil, {redirect_to: public_send("root_path")})
    end

    return if performed?
  end

  def new
  end

  def create
    create_or_update('create')
  end

  def update
    create_or_update('update')
  end

  def create_or_update(action_str)
    # binding.pry
    @site = Site.new(send("#{action_str}_params"))

    @site = set_new_site_defaults @site

    # validation
    if @site.valid?
      api_params = @site.attributes
      api_params.delete('revenueShare') if Site::CPM_ONLY


      @response = create_or_update_site(api_params, action_str)
      response_handler
    else
      @validate_errors = true
    end

    if @api_success
      api_success_handler({redirect_to: publisher_sites_path}, t("site_#{action_str}_success"))
    else

      if action_str == 'create'
        errors_resolver(@site, {render: :new})
      else
        # так как на списке у нас в формах объект для создания: @zone - по имени модели
        # а для редатирования: @edit_obj
        @edit_obj = @site
        errors_resolver(@edit_obj, {render: :edit})
      end

    end

    return if performed?
  end

  def edit
    @form_data_action = "#{@obj_name_plur}_update"
    @form_id = "#{@form_data_action}_form"
    @form_method = 'put'

    @edit_obj = @obj_name.camelize.constantize.new(edit_params)

    # Категории для сайта
    get_categories

    render layout: false
  end

  def destroy
    @response = delete_site(params[:id])
    response_handler

    if @api_success
      api_success_handler({redirect_to: publisher_sites_path}, t("delete_#{@obj_name}_success"))
    else
      errors_resolver(nil, {redirect_to: publisher_sites_path})
    end

    return if performed?
  end

  private

  def create_params
    ActionController::Parameters.new(params).require(:site).permit(
        :id,
        :name,
        :url,
        :description,
        :email,
        :allowPlacementBannersLinkingChange,
        :revenueShare,
        :impressionsByMonth,
        :visitorsByMonth,
        :createDefaultZone,
        :categoryId => []
    )
  end

  def update_params
    create_params
  end

  def edit_params
    ActionController::Parameters.new(params).require(:edit_obj).permit(
        :id,
        :name,
        :url,
        :description,
        :email,
        :impressionsByMonth,
        :visitorsByMonth,
        :categoryId => []
    )
  end


  def set_new_site_defaults obj
    # не разрешаем паблишеру самостоятельно крепить к себе баннеры
    obj[:allowPlacementBannersLinkingChange] = false

    obj
  end

  def set_sites_hash_by_id sites
    obj = {}
    sites.each do |site|
      obj[site['id']] = site
    end

    gon.push({publisher: {sites: obj} })
  end

  def get_categories
    # Категории для сайта
    @response = get_publishing_categories
    response_handler
    @pub_categories = @response
  end
end
