class Publisher::ZonesController < Publisher::MainController

  include PublisherZonesApi
  include PublisherPlacementsApi

  before_action :set_obj_name, only: [:index, :edit, :destroy]

  def index
    # такое же название формируется при обработке ошибок и передается в js
    @form_data_action = "#{@obj_name_plur}_create"
    @form_id = "#{@form_data_action}_form"
    @form_method = 'post'

    # ADD ZONE | для создания нового объекта в модальном окне
    instance_variable_set("@#{@obj_name}", @obj_name.camelize.constantize.new) # @zone = Zone.new

    # GET ZONES for Site
    @siteId = params['siteId']

    # Placement obj
    @placement = Placement.new

    @response = get_zones(current_user.username, current_user.hashed_password, @siteId)
    response_handler

    if @api_success
      @zones = @response.uniq
      current_site_zones_hash(@zones)
    else
      errors_resolver(nil, {redirect_to: publisher_sites_path})
    end

    return if performed?
  end

  def new
  end

  def create
    create_or_update('create')
  end

  def update
    create_or_update('update')
  end

  def create_or_update(action_str)
    @zone = Zone.new(send("#{action_str}_params"))

    # validation
    if @zone.valid?
      api_params = @zone.attributes

      @response = create_or_update_zone(api_params, action_str)
      response_handler

      #  далее нужно будет объединить данные от зоны и от плэйсмента
      @zone_response = @response
    else
      @validate_errors = true
    end


    if @api_success

      # работаем с плэйсментом уже без обработки ошибок так как там все обязательные парметры стоят по дефолту
      @placement = Placement.new(send("placement_#{action_str}_params"))
      api_params = @placement.attributes
      api_params['zoneId'] = @zone_response['id']

      @response = create_or_update_placement(api_params, action_str)
      response_handler

      if @api_success
        # от апи получили только id placement
        placement_id = @response['id']

        # берем ответ зоны там ее id и добавляем placement_id
        @response = @zone_response
        # убрано так как плэйсмент id при редактировании все равно обычно дергаем по апи
        # @response['placement_id'] = placement_id

        api_success_handler({redirect_to: publisher_zones_path( siteId: params.require(:zone)[:siteId] )}, t("zone_#{action_str}_success"))
      else
        if action_str == 'create'
          errors_resolver(@zone, {render: :new})
        else
          @edit_obj = @zone
          @edit_placement = @placement
          errors_resolver(@edit_obj, {render: :edit})
        end
      end

    else

      if action_str == 'create'
        errors_resolver(@zone, {render: :new})
      else
        # так как на списке у нас в формах объект для создания: @zone - по имени модели
        # а для редатирования: @edit_obj
        @edit_obj = @zone
        errors_resolver(@edit_obj, {render: :edit})
      end

    end

    return if performed?
  end

  def edit
    @form_data_action = "#{@obj_name_plur}_update"
    @form_id = "#{@form_data_action}_form"
    @form_method = 'put'

    @siteId = params['siteId']

    @edit_obj = @obj_name.camelize.constantize.new(edit_params)
    @edit_placement = Placement.new(placement_edit_params)

    # Для получения id плэйсмента для редактирования
    @response = get_zone_placements(@edit_obj.id)
    response_handler

    if @api_success && ((@response.is_a? Array) && (@response.length == 0))
      @api_success = false
      @custom_msg = t :zone_not_found
    end

    if  @api_success
      # мы соединили 1 плэйсмент для 1-й зоны поэтому берем первый id из массива
      @edit_placement['id'] = @response[0]['id']

      render layout: false
    else
      #  так как запрос на модалку не .json то ответ ошибки не пройдет по format.json - мы насильно говорим что ответ дать в форме json
      @force_json_response = true
      errors_resolver(@zone, {redirect_to: publisher_zones_path})
    end
  end

  def destroy
    @response = delete_zone(params[:id])
    response_handler

    if @api_success
      api_success_handler({redirect_to: publisher_zones_path( siteId: params[:siteId] )}, t("delete_#{@obj_name}_success"))
    else
      errors_resolver(nil, {redirect_to: publisher_zones_path( siteId: params[:siteId] )})
    end

    return if performed?
  end

  # INVOCATION CODE
  def get_code
    # Для получения id плэйсмента
    @response = get_zone_placements(params['id'])
    response_handler

    if @api_success
      placement_id = @response[0]['id']
      @epom_key = build_epom_key(params['siteId'], params['id'], placement_id)
      render layout: false
    else
      @force_json_response = true
      errors_resolver(nil, {redirect_to: publisher_zones_path})
    end

    return if performed?
  end

  private


  # ZONE PARAMS--------------------------------------
  def create_params
    params.require(:zone).permit(
        :id,
        :name,
        :description,
        :siteId
    )
  end

  def update_params
    create_params
  end

  def edit_params
    params.require(:edit_obj).permit(
        :id,
        :name,
        :description,
        :siteId
    )
  end



  # PLACEMENT PARAMS----------------------------------
  def placement_create_params
    params.require(:zone).permit(
        :placement_id,
        :name,          # это будет имя и описание зоны но это нормально пусть совпадают - юзер видит только имя зоны
        :description,   #
        :zoneId,
        :size_adUnit,
        :defaultCode,
        :size_height,
        :size_width
    )
  end

  def placement_update_params
    p = placement_create_params

    # эта часть параметров - относится к плэйсмент но так как мы берем парамсы из create -  - это id зоны - заменяем на placement_id
    p['id'] = params.require(:zone)['placement_id']
    p.delete 'placement_id'

    p
  end

  def placement_edit_params
    # ! edit_obj
    params.require(:edit_obj).permit(
        :id,
        :name,
        :description,
        :zoneId,
        :size_adUnit,
        :defaultCode,
        :size_height,
        :size_width

    )
  end


  def current_site_zones_hash zones
    obj = {}
    zones.each do |zone|
      obj[zone['id']] = zones
    end

    gon.push({publisher: {current_site: {zones: obj} }})
  end

  def build_epom_key(site_id, zone_id, placement_id)
    str = "#{site_id}|#{zone_id}|#{placement_id}"
    md5_hash(str)
  end

  def build_inv_code
  end
end
