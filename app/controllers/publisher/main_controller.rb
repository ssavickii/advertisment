class Publisher::MainController < ApplicationController

  before_action :check_access
  before_action :check_custom_flash, only: :index

  include PublisherSitesApi

  layout 'application'

  def index
  end

  private

  def check_access
    redirect_to sign_in_users_path, flash: { error: t(:unauthenticated, scope: [:devise, :failure]) } if (!current_user || !current_user.role_publisher?)
  end
end