// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//

// INIT
//= require main/init
//= require main/debug

//= require jquery
//= require jquery_ujs

//Mobile debugging
// require weinre-rails
// require weinre-init

// require twitter/bootstrap
// require danthes

// Vendor Libs ===============
// require jquery-1.8.3.min
//= require jquery.selectbox.min
//= require jquery-ui.min
//= require jquery.jscrollpane
//= require jquery.mousewheel
//= require checkbox
//= require jquery.reveal
//= require highcharts
//= require data
//= require exporting
// END Vendor Libs ===============


// LOAD FIRST
//= require inner/modals
//= require main/flash
//= require main/tooltip
//= require main/errors

//= require inner/charts.js

//= require_directory ./main
//= require_directory ./inner
// require_tree .

