$ ->

  Traffex.tabs = {

    bindEvents: (current_tab) ->
      scope_id = current_tab.data('scope')
      scope_block = $(scope_id)

      tab_links = current_tab.find('li')
      tab_links.on 'click', ->
        link = $(@)
        return false if link.hasClass('current')

        console.log('switch')
        link.addClass('current').siblings().removeClass('current')
        target_id = link.data('target')
        target_tab = $(target_id)

        scope_block.find('.custom_tab').hide()
        target_tab.fadeIn(500).show();
        target_tab.addClass('current').siblings().removeClass('current')

    init: ->
      obj = this
      all_tabs = $('.custom_tabs')

      if all_tabs.length > 0
        all_tabs.each ->
          current_tab = $(@)
          obj.bindEvents(current_tab)

  }

  Traffex.tabs.init()