$ ->
  Traffex.tooltip = {

    default_position: { my: "left+35 center", at: "right center" }

    add: (el, items, content, position=null) ->
      obj = this

      el.tooltip({
        items: items
        position: (if (position != null) then position else obj.default_position)
        content: content
      })

    create: (el, items) ->
      obj = this
      el.tooltip({
        items: items
        position: obj.default_position
        #extraClass: "custom"
      })

    init: ->
      obj = this
      $('.tooltip_on_load').each ->
        obj.create($(@), $(@).get(0).tagName.toLowerCase())
  }

  Traffex.tooltip.init()
