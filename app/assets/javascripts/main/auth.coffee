$ ->

  # SIGN UP MODAL AJAX ----------------------------------------------
  $("#sign_up").on("ajax:success", (e, data, status, xhr) ->
    console.log(data)

    if data.success
      Traffex.errors.clearFormErrors()
      console.log('success sign_up')
      Traffex.flash.close_modal_and_show(data.msg, 'notice')

    else
      if data.hasOwnProperty('form_errors')
        Traffex.errors.handleErrors(data.form_errors, data.form_data_action)

      if data.hasOwnProperty('msg')
        Traffex.errors.clearFormErrors()
        Traffex.flash.close_modal_and_show(data.msg, 'error')
        console.log('FLASH: msg')
  ).on "ajax:error", (e, xhr, status, error) ->
    console.log('auth error')
    Traffex.flash.close_modal_and_show('Ошибка запроса', 'error', true)


  # LOGIN MODAL AJAX -------------------------------------------------
  $("#sign_in").on("ajax:success", (e, data, status, xhr) ->
    console.log(data)

    if data.success
      Traffex.errors.clearFormErrors()
      console.log('success login')

      if (data.hasOwnProperty('action'))
        location.href = data['action']['redirect_to']

    else
      if data.hasOwnProperty('form_errors')
        console.log(data.form_data_action)
        Traffex.errors.handleErrors(data.form_errors, data.form_data_action)

      if data.hasOwnProperty('msg')
        Traffex.errors.clearFormErrors()
        Traffex.flash.close_modal_and_show(data.msg, 'error')
        console.log('FLASH: msg')
  ).on "ajax:error", (e, xhr, status, error) ->
    console.log('login error')
    Traffex.flash.close_modal_and_show('Ошибка запроса', 'error', true)