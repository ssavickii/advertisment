$ ->
  Traffex.flash = {
    close: (this_) ->
      $(this_).hide()
      $(this_).closest('.alert').slideUp()

    hide: (flash) ->
      flash.find('.fa-times').hide()
      flash.slideUp()

    show_js: (msg, class_name, default_error_msg=false) ->
      obj = this

      if default_error_msg
        msg = gon.defaults.default_error_msg
      block = $('#flash_js')
      block.find('#flash_msg').text(msg)
      block.addClass('alert-' + class_name)
      block.show()
      block.find('.fa-times').show()

      obj.hide_timeout(block)

    close_modal_and_show: (msg, class_name, default_error_msg=false) ->
      obj = this
      $('.reveal-modal').each ->
        $(@).trigger('reveal:close')

      obj.show_js(msg, class_name, default_error_msg)

    hide_timeout: (flashes) ->
      obj = this
      flashes.each ->
        flash = $(@)
        setTimeout( ->
          obj.hide(flash)
        , 10000)

    init: ->
      obj = this
      flashes = $('.alert')
      if flashes.length > 0
        obj.hide_timeout(flashes)

  }

  Traffex.flash.init()