$ ->

  Traffex.errors = {

    clearFormErrors: ->
      error_fields = $('[data-validate="true"]').find('.error')
      error_fields.tooltip().tooltip('destroy')
      error_fields.removeClass('error')

    showErrorOn: (el, items, msg) ->
      el.addClass('error')
      Traffex.tooltip.add(el, items, msg)

    showError: (el, items, msg)->
      obj = this
      if items == 'select'
        interval = setInterval( ->
          el = el.prev('.selectbox')
          if el.length > 0
            clearInterval(interval)

            items = 'span'
            obj.showErrorOn(el, items, msg)
        , 100)
      else
        obj.showErrorOn(el, items, msg)

    handleErrors: (form_errors, form_data_action)->
      return unless form_errors

      obj = this
      obj.clearFormErrors()

      form = $('[data-action="' + form_data_action + '"][data-validate="true"]')
      if form.length > 0
        console.log('errors')

        for name, errors of form_errors
          unless (errors.constructor == Array)
            errors = [errors]

          msg = errors.join('<br>').trim()

          el = form.find('[name*=' + name + ']:visible').first()
          if el.length > 0
            obj.showError(el, el.get(0).tagName.toLowerCase(), msg)

          # lkz мультиселектов и прочих кастомных элементов форм - ставить data-validation-name
          el_custom = form.find('[data-validation-name*=' + name + ']:visible').first()
          if el_custom.length > 0
            obj.showError(el_custom, el_custom.get(0).tagName.toLowerCase(), msg)
    init: ->
      obj = this
      if (typeof gon != 'undefined') && gon.hasOwnProperty('form_errors')
        obj.handleErrors(gon.form_errors, gon.form_data_action)
  }

  Traffex.errors.init()