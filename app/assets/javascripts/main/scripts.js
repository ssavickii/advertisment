var global, windowWidth, windowHeight;

$(document).ready(function(){
	
	global = {
			window: $(window),
			body: $('body')
	};
	
	global.window.resize(onResize);
	onResize();

	
	//show/hide banner size
	$('.upload_link').click(function(){
		$('.upload_block').slideDown('fast');
	});
	$('.close').click(function(){
		$('.upload_block').slideUp('fast');
	});
	

	//activate/deactivate "End date"
	$('#end_date').prop('checked', false);
	if($('#end_date').is(":checked")) {
		$('#datepicker_end').removeClass('inp_dis');
		$('#datepicker_end').removeAttr('disabled').val("");
		$('.lab').removeClass('dis');
		$('.arrow_bottom').removeClass('arr_dis');
	}
	else {
		$('.lab').addClass('dis');
		$('.arrow_bottom').addClass('arr_dis');
		$('#datepicker_end').addClass('inp_dis');
		$('#datepicker_end').attr('disabled','disabled').val('unlimited');
	}
	$('#end_date').click(function(){
		if($(this).is(":checked")) {
			$('#datepicker_end').removeClass('inp_dis');
			$('#datepicker_end').removeAttr('disabled').val("");
			$('.lab').removeClass('dis');
			$('.arrow_bottom').removeClass('arr_dis');
		}
		else {
			$('.lab').addClass('dis');
			$('.arrow_bottom').addClass('arr_dis');
			$('#datepicker_end').addClass('inp_dis');
			$('#datepicker_end').attr('disabled','disabled').val('unlimited');
		}
	});
	

	//select country
	$('.sources_list li').on('click', function(){
    var li = $(this);
    var inp = li.find('input');

      if(!inp.prop('checked')){
        $(this).removeClass('selected_item');
      } else {
        $(this).addClass('selected_item');
      }
    });


	//choose checkboxes group
	$('.select_group').click(function(){
		toggleAll(this);
	});
	
	
	//selected file
	$('.form input[type=file]').on('change', function() {
		$(this).parent('li').addClass('file_selected');
	});
	
	
	//select banner size
	$('.size2 .fileInputText').click(function(){
		if($(this).parent('div.mask').hasClass('selected')){
			$(this).parent('div.mask').removeClass('selected');
		}
		else{
			$(this).parent('div.mask').addClass('selected');
		}
	});
	
	
	//load content
	$('#call_js_elem').click(function(){
		$("#code_1").lebnikLoad("#content_from", "1.html");
	});
	
	
	//world map
	$('#map1').hover(function(){		
		$('.map_active').removeClass().addClass('map_1_on');
		$('#stat_1').css('display','block');
		$('#stat_1').hover(function(){			
				$('.map_active').addClass('map_1_on');
				$(this).css('display', 'block');
		},function(){
			$('#map1').mouseout(function(){
				$('#stat_1').css('display', 'none');
			});
		});
		},function(){
		$('#stat_1').css('display','none');	
		
	});
	
	$('#map2').hover(function(){		
		$('.map_active').removeClass().addClass('map_2_on');
		$('#stat_2').css('display','block');
		$('#stat_2').hover(function(){			
				$('.map_active').addClass('map_2_on');
				$(this).css('display', 'block');
		});
		},function(){
		$('#stat_2').css('display','none');	
		
	});
	
	$('#map3').hover(function(){		
		$('.map_active').removeClass().addClass('map_3_on');
		$('#stat_3').css('display','block');
		$('#stat_3').hover(function(){			
				$('.map_active').addClass('map_3_on');
				$(this).css('display', 'block');
		});
		},function(){
		$('#stat_3').css('display','none');	
		
	});
	
	$('#map4').hover(function(){		
		$('.map_active').removeClass().addClass('map_4_on');
		$('#stat_4').css('display','block');
		$('#stat_4').hover(function(){			
				$('.map_active').addClass('map_4_on');
				$(this).css('display', 'block');
		});
		},function(){
		$('#stat_4').css('display','none');	
		
	});
	
	$('#map5').hover(function(){		
		$('.map_active').removeClass().addClass('map_5_on');
		$('#stat_5').css('display','block');
		$('#stat_5').hover(function(){			
				$('.map_active').addClass('map_5_on');
				$(this).css('display', 'block');
		});
		},function(){
		$('#stat_5').css('display','none');	
		
	});
	
	/*---*/
	
	$('#pub_map1').hover(function(){		
		$('.pub_map_active').removeClass().addClass('pub_map_1_on');
		$('#pub_stat_1').css('display','block');
		$('#pub_stat_1').hover(function(){			
				$('.pub_map_active').addClass('pub_map_1_on');
				$(this).css('display', 'block');
		},function(){
			$('#pub_map1').mouseout(function(){
				$('#pub_stat_1').css('display', 'none');
			});
		});
		},function(){
		$('#pub_stat_1').css('display','none');	
		
	});
	
	$('#pub_map2').hover(function(){		
		$('.pub_map_active').removeClass().addClass('pub_map_2_on');
		$('#pub_stat_2').css('display','block');
		$('#pub_stat_2').hover(function(){			
				$('.pub_map_active').addClass('pub_map_2_on');
				$(this).css('display', 'block');
		});
		},function(){
		$('#pub_stat_2').css('display','none');	
		
	});
	
	$('#pub_map3').hover(function(){		
		$('.pub_map_active').removeClass().addClass('pub_map_3_on');
		$('#pub_stat_3').css('display','block');
		$('#pub_stat_3').hover(function(){			
				$('.pub_map_active').addClass('pub_map_3_on');
				$(this).css('display', 'block');
		});
		},function(){
		$('#pub_stat_3').css('display','none');	
		
	});
	
	$('#pub_map4').hover(function(){		
		$('.pub_map_active').removeClass().addClass('pub_map_4_on');
		$('#pub_stat_4').css('display','block');
		$('#pub_stat_4').hover(function(){			
				$('.pub_map_active').addClass('pub_map_4_on');
				$(this).css('display', 'block');
		});
		},function(){
		$('#pub_stat_4').css('display','none');	
		
	});
	
	$('#pub_map5').hover(function(){		
		$('.pub_map_active').removeClass().addClass('pub_map_5_on');
		$('#pub_stat_5').css('display','block');
		$('#pub_stat_5').hover(function(){			
				$('.pub_map_active').addClass('pub_map_5_on');
				$(this).css('display', 'block');
		});
		},function(){
		$('#pub_stat_5').css('display','none');	
		
	});
	
	
	
	//
	$(".arr").click(function() {
		$("html, body").animate({
				scrollTop: $($(this).attr("href")).offset().top
		}, {
				duration: 500
		});
		return false;
	});
	
	
	//
	$('.email').click(function(){
		$('.email').removeClass('mail_active');
		$(this).addClass('mail_active');
	});
	
	
	//Placeholder for IE
//	if($.browser.msie) {
	if(gon.browser.ie) {
		$("form").find("input[type='text']").each(function() {
				var tp = $(this).attr("placeholder");
				$(this).attr('value',tp).css('color','#717171');
		}).focusin(function() {
				var val = $(this).attr('placeholder');
				if($(this).val() == val) {
						$(this).attr('value','').css('color','#717171');
				}
		}).focusout(function() {
				var val = $(this).attr('placeholder');
				if($(this).val() == "") {
						$(this).attr('value', val).css('color','#717171');
				}
		});
		/* Protected send form */
		$("form").submit(function() {
				$(this).find("input[type='text']").each(function() {
						var val = $(this).attr('placeholder');
						if($(this).val() == val) {
								$(this).attr('value','');
						}
				})
		});
		$("form").find("textarea").each(function() {
				var tp = $(this).attr("placeholder");
				$(this).attr('value',tp).css('color','#717171');
		}).focusin(function() {
				var val = $(this).attr('placeholder');
				if($(this).val() == val) {
						$(this).attr('value','').css('color','#717171');
				}
		}).focusout(function() {
				var val = $(this).attr('placeholder');
				if($(this).val() == "") {
						$(this).attr('value', val).css('color','#717171');
				}
		});
		/* Protected send form */
		$("form").submit(function() {
				$(this).find("textarea").each(function() {
						var val = $(this).attr('placeholder');
						if($(this).val() == val) {
								$(this).attr('value','');
						}
				})
		});
	}
	
	
	//last-child for IE8
	$('.advert_list li:last-child').addClass('last-child');
	$('.pp_form .sources_list2 li:first-child').addClass('first-child');
	$('.pp_form .sources_list2 li:last-child').addClass('last-child');
	$('.filter .sources_list2 li:first-child').addClass('first-child');
	$('.filter .sources_list2 li:last-child').addClass('last-child');
	$('.source2 .sources_list2 li:first-child').addClass('first-child');
	$('.source2 .sources_list2 li:last-child').addClass('last-child');
	
	
	//filter
	$('.filter_off').click(function(){
		if($(this).hasClass('filter_on')){
			$(this).removeClass('filter_on');
			$(this).next('.filter_content').css('display', 'none');
			rightPanelHeight();
		}
		else{
			$(this).addClass('filter_on');
			$(this).next('.filter_content').css('display', 'table');
			rightPanelHeight();
		}
	});
	
	//filter scale
	$( "#tr_1" ).slider({
			range: "max",
			min: 0,
			max: 48,
			value: 0,
			slide: function( event, ui ) {$( "#amount_1" ).val( ui.value );}
	});
	
});


function onResize(){
	windowWidth = global.window.width();
	windowHeight = global.window.height();
	
	/*if(windowWidth <= 1292){
		$('.wrapper').css({'min-width': '1255px', 'width': '1255px'});
		$('.inner_body .wrapper').css({'min-width': '1255px', 'width': '100%'});
		$('.wrap').css({'width': '1255px'});
		$('.tabs_block').css({'margin-right': '0px'});
		$('.content_in').css({'padding-left': '15px', 'padding-right': '15px'});
		$('footer').css({'width': '1255px'});
		$('footer.main_foot').css({'width': '100%'});
		$('.right_panel').css({'width': '230px', 'right': '0px'});
	}
	else{
		$('.wrapper').css({'min-width': '1292px', 'width': '1292px'});
		$('.inner_body .wrapper').css({'min-width': '1292px', 'width': '100%'});
		$('.wrap').css({'width': '1292px'});
		$('.tabs_block').css({'margin-right': '1px'});
		$('.content_in').css({'padding-left': '35px', 'padding-right': '35px'});
		$('footer').css({'width': '1292px'});
		$('footer.main_foot').css({'width': '100%'});
		$('.right_panel').css({'width': '240px', 'right': '1px'});
	}*/
	
	rightPanelHeight();
	
}

// select
(function($) {
	$(function() {
		$('select').selectbox();
	})
})(jQuery)


//calendar
$(function() {
	$( ".date" ).datepicker();
});


//select checkboxes group
function toggleAll(obj){
	$(obj).next('ul').find('li input:checkbox:enabled').attr
	(
		"checked",
		function()
		{
			return !this.checked;
		}
	);
}


//check panel height
function rightPanelHeight(){
	if(windowHeight > $('.content').height())
		$('.right_panel').css('height', windowHeight + 'px');
	else
		$('.right_panel').css('height', $('.content').height() + 'px');
}


//tabs
(function($) {
  $(function() {

    // MAIN MENU TABS
    $('ul.tabs:not(.custom)').delegate('li:not(.current)', 'click', function() {
      var tab_li = $(this);

      if (tab_li.hasClass('static')) {
        // переход по статичной ссылке
      } else {
        tab_li.addClass('current').siblings().removeClass('current');

        var target = tab_li.data('target');
        $('.tab-content-js').hide();
        $(target).fadeIn(500).show();

        console.log('tab');

        rightPanelHeight();
        $('.scrollblock').jScrollPane();
        return false;
      }

    });

    // INNER TABS
    $('ul.inner-tabs').delegate('li:not(.current)', 'click', function() {
      var tab_li = $(this);
      if (tab_li.hasClass('static')) {
        // переход по статичной ссылке
      } else {
        tab_li.addClass('current').siblings().removeClass('current');

        var target = tab_li.data('target');
        $('.inner-tab-content-js').hide();
        $(target).fadeIn(500).show();

        console.log('tab')

        rightPanelHeight();
        $('.scrollblock').jScrollPane();
        return false;
      }
    });

  });
})(jQuery);


//upload file content
$(function($) {
	$.lebnikLoad = function(selector, url, callback){ $(document.body).lebnikLoad(selector, url, callback, true); };
	$.fn.lebnikLoad = function(selector, url, callback, without_selector_document){
			var selector_document = this;
			var e = $('<iframe style="display:none" src="'+url+'"></iframe>');
			$(document.body).append( e );
			$(e).load(function(){
					var x = $(selector, e[0].contentWindow.document);
					if(callback){
							callback(x);
					}else if(without_selector_document != true){
							$(selector_document).html( $(x).html() );
					}
			});
	};
});


function clear_all(){
	for(var i = 1; i <= 9; i++){
		$('#stat_'+i).css('display','none');	
	}
}

function clear_all_pub(){
	for(var i = 1; i <= 9; i++){
		$('#pub_stat_'+i).css('display','none');	
	}
}



// list in 7 columns
$(function(){
var updateColl
jQuery.fn.liColl = function(options){
    // настройки по умолчанию
    var o = jQuery.extend({
        n_coll: 7, //колличество колонок
        c_unit: 'px', //Единица измерения
        c_width: 102, //Ширина колонок
        p_left: 0 //отступ слева %
    },options);
    updateColl = function update(listWrap){
        listWrap.children('.coll_s').each(function(){
            $(this).children().unwrap();    
        })
        listWrap.liColl(options);
    }
    return this.each(function(){
        element = jQuery(this).css({overflow:'hidden'});
        nc = o.n_coll;
        pl = o.p_left;
        i = 1;
        c_un = 'px';
        if(options.c_unit != c_un){
            coll_w = Math.floor(100/nc);
            coll_w = coll_w - pl;
        }else{
            coll_w = options.c_width - pl;
        };
        num_1 = element.children('li').length;
        create();
        function create(){
            n_end = Math.ceil(num_1/nc);
            var cc = jQuery('<div />').addClass("coll_s c_" + i).css({width:coll_w+options.c_unit,paddingLeft:pl+options.c_unit,float:'left',clear:'right'})
            //element_2.append();
            element.children('li').slice(0,n_end).wrapAll(cc);
            if(num_1 != n_end){
                i++;
                nc--;
                num_1 = num_1 - n_end;
                create();
            };
        };
    });
};

$('.list_coll').liColl({
        c_unit: 'px', // '%' или 'px' При указании '%' — ширина 'c_width' игнорируется
        n_coll: 7, //колличество колонок
        p_left: 0 //отступ слева %                        
    });
    $('.list_coll-2').liColl({
        c_unit: 'px', // '%' или 'px' При указании '%' — ширина 'c_width' игнорируется
        n_coll: 7, //колличество колонок
        c_width: 102, //Ширина колонок в 'px'
        p_left: 0 //отступ слева %                        
    });
    
    //test script
    $('.add-new').click(function(){
        $('<li>').text('New').appendTo('.list_coll-2')    
        updateColl($('.list_coll-2'))
    })

})