$ ->

  # STANDARD IN MODAL ----------------------------------------------

  # на примере Zone
  # в контроллере мы задаем все переменные которые нужны для форм создания и редактирования
  # в паршал списка мы передаем имена связанные с именем объекта(его контроллера)
  # ячейки списка именуются в соответствии с именам полей формы - так как в ответах от апи мало информации
  # после успешного создания клон строки заполняется в соответствии с данными формы
  # важно просто чтобы аттрибуты форм, таблицы со списком были верные
  # аттрибуты форм подстроены под конструктор имени form_data_action при обработке ошибок
  # пример:
  # debug pass action: zones_create
  # (controller: zones, action: create)
  # create form:  id="zones_create_form" data-action="zones_create"
  # update form:  id="zones_update_form" data-action="zones_update"
  # table list:   id="zones_created_list"

  Traffex.standard = {
    standard_forms: $("[data-standard-action]")

    markAsLoaded: (modal, loaded_name) ->
      modal.data('loaded', loaded_name)

    editModalEvents: (modal) ->
      obj = this
      # CHECKBOXES
      onload()
      console.log('рефреш мультичекбоксов')

      # show modal html
      modal.reveal({animation: 'fade'})

      # update event
      obj.afterUpdateByModal(modal.find('form'))


  # CREATE IN MODAL ----------------------------------------------
    createInModal: (form) ->
      form.on("ajax:success", (e, data, status, xhr) ->
        console.log(data)

        if data.success
          Traffex.errors.clearFormErrors()
          console.log('success create object')
          Traffex.flash.close_modal_and_show(data.msg, 'notice')

          # CUSTOM CALLBACK FOR each FORM type
          Traffex.forms.customCallback(form, data)

        else
          if data.hasOwnProperty('form_errors')
            Traffex.errors.handleErrors(data.form_errors, data.form_data_action)

          if data.hasOwnProperty('msg')
            Traffex.errors.clearFormErrors()
            Traffex.flash.close_modal_and_show(data.msg, 'error')
            console.log('FLASH: msg')

      ).on "ajax:error", (e, xhr, status, error) ->
        console.log('ajax error')
        Traffex.flash.close_modal_and_show('Ошибка запроса', 'error', true)


    afterUpdateByModal: (form) ->
      form.on("ajax:success", (e, data, status, xhr) ->
        console.log(data)

        if data.success
          Traffex.errors.clearFormErrors()
          console.log('success update object')
          Traffex.flash.close_modal_and_show(data.msg, 'notice')

          # CUSTOM CALLBACK FOR each FORM type
          Traffex.forms.customCallback(form, data)

        else
          if data.hasOwnProperty('form_errors')
            Traffex.errors.handleErrors(data.form_errors, data.form_data_action)
            # TODO SELECT Errors for multy select
          if data.hasOwnProperty('msg')
            Traffex.errors.clearFormErrors()
            Traffex.flash.close_modal_and_show(data.msg, 'error')
            console.log('FLASH: msg')

      ).on "ajax:error", (e, xhr, status, error) ->
        console.log('ajax error')
        Traffex.flash.close_modal_and_show('Ошибка запроса', 'error', true)


    editInModal: (link, id) ->
      obj = this
      # get html for modal
      console.log('standart edit object in Modal with id: ' + id)

      link_data = link.data()
      url = link_data['href']
      params = link_data['params']

      modal_id = '#edit' + link_data['modelNameCamelize'] + 'Modal'
      modal = $(modal_id)

      loaded_name = link_data['modelName'] + '_' + id

      # Проверяем были ли уже загружены данные в модалку
      if modal.data('loaded') == loaded_name
        obj.editModalEvents(modal)
        return false

      console.log('get edit form by ajax')
      $.ajax
        url: url
        type: 'GET'
        data: params
        cache: false
        success: (data) ->
          if data.hasOwnProperty('success') && (data['success'] == false)
            if data.hasOwnProperty('msg')
              Traffex.flash.close_modal_and_show(data.msg, 'error')
              console.log('FLASH: msg')
            else
              Traffex.flash.show_js('Ошибка запроса', 'error', true)
          else
            console.log('success get edit site')
            #    Traffex.modals.offsetTop = 30
            modal.find('.modal_content').html(data)

            obj.editModalEvents(modal)

            obj.markAsLoaded(modal, loaded_name)

        error:  (e, xhr, status, error) ->
          console.log('ajax error')
          Traffex.flash.show_js('Ошибка запроса', 'error', true)

    deleteObjEvent: (link) ->
      link.on("ajax:success", (e, data, status, xhr) ->
        console.log(data)

        if data.success
          console.log('success delete object')
          Traffex.flash.show_js(data.msg, 'notice')

          # CUSTOM CALLBACK FOR each FORM type
          Traffex.forms.customCallback(link, data)
        else
          if data.hasOwnProperty('msg')
            Traffex.errors.clearFormErrors()
            Traffex.flash.show_js(data.msg, 'error')
            console.log('FLASH: msg')

      ).on "ajax:error", (e, xhr, status, error) ->
        console.log('ajax error')
        Traffex.flash.show_js('Ошибка запроса', 'error', true)


    init: ->
      obj = this

      if obj.standard_forms.length > 0

        obj.standard_forms.each ->
          form = $(@)

          # CREATE IN MODAL ----------------------------------------------
          if form.data('standard-action') == 'create'
            obj.createInModal(form)

          # Update IN MODAL ----------------------------------------------
          # для этого форму в модал получаем аякс запросом сначала - editInModal - стоит в верстке на ссылке

          # DELETE ----------------------------------------------
          if form.data('standard-action') == 'delete'
            obj.deleteObjEvent(form)
            # form - на самом деле link data-standard-action='delete'




  }

  Traffex.standard.init()



