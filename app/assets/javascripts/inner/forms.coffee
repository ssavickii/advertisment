$ ->
  # FORM CALLBACKS

  Traffex.forms = {

  # для подстановки полей нового объекта в список
    buildFormHash: (action) ->
      output_hash = {}
      from_array = $('#' + action + '_form').serializeArray()
      for el in from_array
        output_hash[el['name']] = el['value']
      return output_hash

    customCallback: (form_or_link, data) ->
      action = form_or_link.data('action')

      if action == 'modelname_create'
        # example:
        #Traffex.zones.createZoneCallback(form_or_link, action, data)

      else

        # STANDARD CREATE CALLBACK
        if /create/.test(action)
          Traffex.callbacks.createCallback(form_or_link, action, data)

        # STANDARD UPDATE CALLBACK
        else if /update/.test(action)
          Traffex.callbacks.updateCallback(form_or_link, action, data)

        # STANDARD DELETE CALLBACK
        else if /delete/.test(action)
          Traffex.callbacks.deleteCallback(form_or_link, action, data)


  }