$ ->

  Traffex.callbacks = {

    zoneEditLinkParams: (response, form_hash, model_name, edit_link) ->
      response['name'] = form_hash[model_name+'[name]']
      edit_link.data('params', {edit_obj: response, siteId: form_hash[model_name+'[siteId]']})
      return edit_link

    siteEditLinkParams: (response, form_hash, model_name, edit_link) ->
      response['name'] = form_hash[model_name+'[name]']

      console.log( {edit_obj: response})
      edit_link.data('params', {edit_obj: response})
      return edit_link


    createCallback: (form, action, data) ->
      obj = this
      console.log('create standard callback')

      if data.hasOwnProperty('response')
        response = data.response

        # данные из формы так как от эпом может придти вообще только id
        form_hash = Traffex.forms.buildFormHash(action)

        model_name = form.data('model-name')

        # Таблица со списком
        table = $('#' + action + 'd_list')
        tr_clone = table.find('#'+model_name+'_tr_template').clone()

        tr_clone.attr('id', response.id)
        tr_clone.removeClass('none')

        # подстановка значений в ячейках таблицы из отправленной ранее формы - так как ответ от эпом мало дает
        tr_clone.find('td').each ->
          td = $(@)
          td_name = td.data('name')

          if typeof td_name != 'undefined'
            if form_hash.hasOwnProperty(td_name)
              td.text(form_hash[td_name])

        # EDIT LINK
        edit_link = tr_clone.find('.edit_'+model_name)

        # data_href
        data_href = edit_link.data('href')
        edit_link.data('href', data_href.replace(/[0-9]+\/edit/, response.id+'/edit'))

        window.edit_link = edit_link

        # onclick
        onclick_attr = edit_link.attr('onclick')
        edit_link.attr('onclick', onclick_attr.replace(/[0-9]+/, response.id))



        # data_params - Эта часть разная---------------------------------------------------------------------------
        # ссылка редактирования содержит параметры передаваемые для формирования формы на сервере - эта часть разная
        if action == 'zones_create'
          edit_link = obj.zoneEditLinkParams(response, form_hash, model_name, edit_link)

        else if action == 'sites_create'
          edit_link = obj.siteEditLinkParams(response, form_hash, model_name, edit_link)

          zones_links = tr_clone.find('.zones_redirect')
          zones_links.each ->
            zones_href = $(@).attr('href')
            $(@).attr('href', zones_href.replace(/siteId=[0-9]+/, 'siteId='+response.id))

        # ---------------------------------------------------------------------------




        # DELETE LINK
        delete_link = tr_clone.find('.delete_'+model_name)
        delete_href = delete_link.attr('href')
        delete_link.attr('href', delete_href.replace(/\/[0-9]+/, '/'+response.id))

        Traffex.standard.deleteObjEvent(delete_link)

        # должно получиться так
        #<a class="st_2 edit_zone" data-href="/publisher/zones/16/edit" data-params="{"edit_obj":{"id":47,"name":"oo"}}" href="javascript: void(0); onclick="Traffex.standard.editInModal(16)"></a>
        # внешне аттрибуты data не меняются но  $(..).data() изменится
        # поэтому все data аттрибуты берем чеоез $().data(..)

        table.find('tbody').append(tr_clone)
        table.removeClass('none')
        table.closest('.list_wrapper').find('.empty_info').addClass('none')

    updateCallback: (form, action, data) ->
      obj = this
      console.log('update standard callback')

      if data.hasOwnProperty('response')
        response = data.response

        form_hash = Traffex.forms.buildFormHash(action)

        model_name = form.data('model-name')
        model_plur_name = form.data('model-plur-name')

        table = $('#' + model_plur_name + '_created_list')

        tr_edited = table.find('tr[id="' + response.id + '"]')

        tr_edited.find('td').each ->
          td = $(@)
          td_name = td.data('name')

          if typeof td_name != 'undefined'
            if form_hash.hasOwnProperty(td_name)
              td.text(form_hash[td_name])

        # EDIT LINK
        edit_link = tr_edited.find('.edit_'+model_name)

        # Эта часть разная---------------------------------------------------------------------------
        if action == 'zones_update'
          edit_link = obj.zoneEditLinkParams(response, form_hash, model_name, edit_link)

        else if action == 'sites_update'
          edit_link = obj.siteEditLinkParams(response, form_hash, model_name, edit_link)

        # ---------------------------------------------------------------------------


    deleteCallback: (link, action, data) ->
      console.log('delete standard callback')
      link.closest('tr').remove()
  }