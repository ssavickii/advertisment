$ ->

  # !!! все data аттрибуты берем и меняем чеоез $().data(..)

  #  CREATE UPDATE DELETE - стандартизовано callbacks.coffee -> customCallback

  Traffex.zones = {

    codeModalEvents: (modal) ->
      console.log('inv code paste callback')
      # show modal html
      modal.reveal({animation: 'fade'})

      tab_block = modal.find('.custom_tabs').first()
      Traffex.tabs.bindEvents(tab_block)

    getCodeInModal: (link, id) ->
      obj = this
      # get html for modal
      console.log('get code')

      link_data = link.data()
      url = link_data['href']
      params = link_data['params']

      modal_id = '#invCodeModal'
      modal = $(modal_id)

      # здесь модель будет - зона и ее id - чтоб пометить как загруженное после
      loaded_name = link_data['modelName'] + '_' + id

      # Проверяем были ли уже загружены данные в модалку
      if modal.data('loaded') == loaded_name
        obj.codeModalEvents(modal)
        return false

      console.log('get code by ajax')
      $.ajax
        url: url
        type: 'GET'
        data: params
        cache: false
        success: (data) ->
          if data.hasOwnProperty('success') && (data['success'] == false)
            if data.hasOwnProperty('msg')
              Traffex.flash.close_modal_and_show(data.msg, 'error')
              console.log('FLASH: msg')
            else
              Traffex.flash.show_js('Ошибка запроса', 'error', true)
          else
            console.log('success get data')
            #    Traffex.modals.offsetTop = 30
            modal.find('.modal_content').html(data)

            obj.codeModalEvents(modal)

            Traffex.standard.markAsLoaded(modal, loaded_name)

        error:  (e, xhr, status, error) ->
          console.log('ajax error')
          Traffex.flash.show_js('Ошибка запроса', 'error', true)

    adUnitSelectEvents: ->
      $('#ad_unit_select').on 'change', ->
        select = $(@)
        size_block = $('#custom_size')

        if select.val() == 'CUSTOM_SITE_BANNER_SIZE'
          size_block.removeClass('none')
        else
          size_block.addClass('none')
          size_block.find('input')
            .attr('value', '')
            .val('')

    init: ->
      obj = this
      obj.adUnitSelectEvents()

  }


  Traffex.zones.init()
