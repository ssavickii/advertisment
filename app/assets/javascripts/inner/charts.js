$(function () {
  $('#container2').highcharts({
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false
    },
    title: {
      text: ''
    },
    tooltip: {
      pointFormat: '<b>{series.name}<br> {point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}<br> {point.percentage:.1f} %</b>',
          style: {
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
        ['Windows 7',   45.0],
        ['Windows Vista',       26.8],
        ['Windows XP',       12.8],
        ['Linux',    8.5],
        ['Other',     6.2],
        ['Mac OS',   0.7]
      ]
    }]
  });
});

$(function () {
  $('#container3').highcharts({
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false
    },
    title: {
      text: ''
    },
    tooltip: {
      pointFormat: '<b>{series.name}<br> {point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}<br> {point.percentage:.1f} %</b>',
          style: {
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
        ['Firefox',   45.0],
        ['IE',       26.8],
        ['Chrome',       12.8],
        ['Safari',    8.5],
        ['Opera',     6.2],
        ['Others',   0.7]
      ]
    }]
  });
});

$(function () {
  $('#container4').highcharts({
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false
    },
    title: {
      text: ''
    },
    tooltip: {
      pointFormat: '<b>{series.name}<br> {point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}<br> {point.percentage:.1f} %</b>',
          style: {
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
        ['Windows 7',   45.0],
        ['Windows Vista',       26.8],
        ['Windows XP',       12.8],
        ['Linux',    8.5],
        ['Other',     6.2],
        ['Mac OS',   0.7]
      ]
    }]
  });
});

$(function () {
  $('#container5').highcharts({
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false
    },
    title: {
      text: ''
    },
    tooltip: {
      pointFormat: '<b>{series.name}<br> {point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}<br> {point.percentage:.1f} %</b>',
          style: {
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
        ['Firefox',   45.0],
        ['IE',       26.8],
        ['Chrome',       12.8],
        ['Safari',    8.5],
        ['Opera',     6.2],
        ['Others',   0.7]
      ]
    }]
  });
});
