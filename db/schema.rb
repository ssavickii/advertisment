# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150325135256) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "placements", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "zoneId"
    t.string   "type",                     default: "SITE_PLACEMENT", null: false
    t.string   "size_adUnit",              default: "LEADERBOARD",    null: false
    t.integer  "size_height"
    t.integer  "size_width"
    t.boolean  "allowVariableBannerSizes", default: false,            null: false
    t.text     "defaultCode"
    t.integer  "rotateInterval"
    t.string   "payment_model",            default: "CPM",            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sites", force: true do |t|
    t.string   "name"
    t.string   "url"
    t.text     "categoryId",                         default: [],                 array: true
    t.text     "description"
    t.string   "email"
    t.boolean  "allowPlacementBannersLinkingChange", default: false, null: false
    t.float    "revenueShare"
    t.string   "impressionsByMonth"
    t.string   "visitorsByMonth"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "password"
    t.string   "email"
    t.string   "role"
    t.string   "phone"
    t.string   "firstName"
    t.string   "lastName"
    t.string   "websiteUrl"
    t.string   "company"
    t.string   "country"
    t.string   "state"
    t.boolean  "enable_market_integration"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "zones", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "siteId"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
