class CreatePlacements < ActiveRecord::Migration
  def change
    create_table :placements do |t|
      t.string    :name
      t.text      :description
      t.integer   :zoneId
      t.string    :type, default: 'SITE_PLACEMENT', null: false

      # TODO - при валидации имена могут по другому называться
      t.string    :size_adUnit, default: 'LEADERBOARD', null: false
      t.integer   :size_height
      t.integer   :size_width

      t.boolean   :allowVariableBannerSizes, default: false, null: false
      t.text      :defaultCode       #  string, code to run when no ad returned (optional)
      t.integer   :rotateInterval    # long between 0 and 1000, seconds between ad change, works only for iframe invocation code (optional)

      t.string    :payment_model, default: 'CPM', null: false

      t.timestamps
    end
  end
end
