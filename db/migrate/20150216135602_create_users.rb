class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username                     # – (required, maximum length is 64 symbols);
      t.string :password                     # – (required, minimum length is 5 symbols, maximum length is 64 symbols);
      t.string :email                        # – (required, maximum length is 64 symbols, valid email address syntax);
      t.string :role                         # – can be one of the roles presented by Epom System(ADVERTISER,PUBLISHER);
      t.string :phone                        # – (required, format like +22(345)7891012, +1 790 123 7788, +23(1123)12-12-334, maximum length is 64 symbols);
      t.string :firstName                    # – (required, maximum length is 64 symbols);
      t.string :lastName                     # – (required, maximum length is 64 symbols);
      t.string :websiteUrl                   # – (required, maximum length is 64 symbols, valid url syntax);

      t.string :company                      # – (not required, maximum length is 64 symbols);
      t.string :country                      # – (not required, two symbol abbreviation);
      t.string :state                        # – (not required, maximum length is 64 symbols);
      t.boolean :enable_market_integration   # – (not required, boolean value);

      t.timestamps
    end
  end
end
