class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|

      t.string  :name                                           # site name (mandatory)
      t.string  :url                                            # site url (mandatory)

      # TODO    ARRAY?
      t.text    :categoryId, array: true, default: []           # id of category site belongs to (mandatory)

      # OPTIONAL
      t.text    :description                                    # a short site characteristic (optional)
      t.string  :email                                          # contact email (optional)
      t.boolean :allowPlacementBannersLinkingChange, default: false, null: false             # boolean value to allow user to perform banner to placements linking itself (optional)
      t.float   :revenueShare                                   # float between 0 and 100 to represent revenue share percent for given site (optional)
      t.string :impressionsByMonth                             # estimation for impressions served per-month,can be one of following (optional): LESS_THAN_100K,_100K_1_MILLION,_1_MILLION_50_MILLION,_50_MILLION_100_MILLION, _100_PLUS_MILLION,NOT_YET_LAUNCHED,DONT_KNOW (mandatory).
      t.string :visitorsByMonth                                # estimation for number of unique visitors of site within month, can be one of following (optional): LESS_THAN_100K,_100K_1_MILLION,_1_MILLION_50_MILLION,_50_MILLION_100_MILLION, _100_PLUS_MILLION,NOT_YET_LAUNCHED,DONT_KNOW (mandatory).

      t.timestamps
    end
  end
end
